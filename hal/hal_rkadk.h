#ifndef __HAL_RKADK_H__
#define __HAL_RKADK_H__

void hal_rkadk_init(lv_coord_t hor_res, lv_coord_t ver_res, int rotated);

#endif

